<?php
namespace app\controllers;
/**
*
*/
class SiteController
{
	/*
	function __construct(argument)
	{
		# code...
	}
*/
	public function checkMethod($name){
		if(method_exists($this, "Action".ucfirst($name))){
			return "Action".ucfirst($name);
		}else{
			return false;
		}
	}

	public function ActionIndex($app, $params = null)
	{
		if($_SERVER['REQUEST_METHOD'] == "GET"){
			$messages = $app->db->getAllMessages();
			$app->render("index", [
				'data' => $messages,
			]);
		}
	}

	public function ActionJsonindex($app, $params = null)
	{
		if($_SERVER['REQUEST_METHOD'] == "GET"){
			$messages = $app->db->getAllMessages();
			echo json_encode($messages);
		}
	}

	public function ActionSendmessage($app, $params = null)
	{
		if($_SERVER['REQUEST_METHOD'] == "GET"){
			$app->render("send-message", []);
		}

		if($_SERVER['REQUEST_METHOD'] == "POST"){
			$fields = [
		  		'username' => [
		  			'type'=>'username',
		  			'required' => false,
		  		],
		  		'email' => [
		  			'type'=>'email',
		  			'required' => true,
		  		],
		  		'text' => [
		  			'type'=>'text',
		  			'required' => true,
		  		],
		  	];

		  	$params = $app->db->validate($fields, $_POST);
		  	if($params){
					$params['datetime'] = time();
		  		$app->db->insert($params, "messages");
				}
		}
	}
}
