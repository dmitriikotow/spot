<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

$_config = require_once(__DIR__ . DIRECTORY_SEPARATOR . "config/config.php");
require_once(__DIR__ . DIRECTORY_SEPARATOR . "class/Application.php");

$app = new app\Application($_config);
$app->go();
