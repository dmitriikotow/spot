function getMessages(grid){
  $.get("index.php?r=site/jsonindex", function( data ) {
    table_rows_obj = JSON.parse(data);
    grid.empty();
    grid.sweetPages({perPage:25, rows: table_rows_obj});
  });
}

function sendMessage(obj){
  var msg=$('#messageform').serialize();
  $(obj).prop("disabled", true );
    $(obj).attr('value', 'Подождите');
  $.ajax({
    type:'POST',
      url:"<?php echo $_SERVER['PHP_SELF']?>?r=site/sendmessage",
    data:msg+"&action=sendmessage",
    cache:false,
    success:function(data){
      $(obj).prop("disabled", false);
        $(obj).attr('value', 'Отправить');
        $('#sendmessage--block').hide(500);
        getMessages();
    }
  });

}

function sortTable	(sort_field){
  var sort_flag = $('.sort-control').find('#ad').prop('checked');
  switch (sort_field) {
    case 'datetime':

      table_rows_obj.sort(function(a, b){

        if(parseInt(a.datetime) > parseInt(b.datetime)){
          return (sort_flag)?1:-1;
        }
        if(parseInt(a.datetime) < parseInt(b.datetime)){
          return (sort_flag)?-1:1;;
        }
        return 0;
    });
      break;
    case 'username':
      table_rows_obj.sort(function(a, b){
        if(a.username == null) a.username = "";
        var res = a.username.localeCompare(b.username);
        return (sort_flag)?res : res * -1;
    });
      break;
    case 'email':
      table_rows_obj.sort(function(a, b){
        if(a.email > b.email){
          return 1;
        }
        if(a.email < b.email){
          return -1;
        }
        return 0;
    });
      break;
    default:
      alert( 'Я таких значений не знаю' );
  }
  $('#holder').empty();
  $('#holder').sweetPages({perPage:10, rows: table_rows_obj});
}
