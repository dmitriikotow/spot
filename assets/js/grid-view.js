﻿(function($){

// Создаем плагин jQuery sweetPages:
$.fn.sweetPages = function(opts){
	
	// Если не было передано никаких опций, создаем пустой объект opts
	if(!opts) opts = {};
	grid = this;
	var resultsPerPage = opts.perPage || 3;
	
	// Плагин лучше всего работает с неупорядоченным списком, хотя ol тоже будет обработан:
	if(opts.rows == undefined){
		var json_test = $('#table-content-hidden').html();
		if(json_test == undefined) return;
		var rows = JSON.parse(json_test);	
	}else{
		var rows = opts.rows
	}

	// Вычисление общего количества страниц:
	var pagesNumber = Math.ceil(rows.length/resultsPerPage);
	
	// Если страниц меньше, чем две, то ничего не делаем:
	//if(pagesNumber<2) return this;

	// Создаем управляющий div:
	var swControls = $('<div class="swControls">');
	
 	var tr_array = [];
	rows.forEach(function(element, index, array){
		var tr = $("<tr>");

		var td = $("<td>");
		td.attr('class', 'username');
		td.html(element.username);
		tr.append(td);

		var td = $("<td>");
		td.attr('class', 'email');
		td.html(element.email);
		tr.append(td);

		var td = $("<td>");
		td.attr('class', 'text');
		td.html(element.text);
		tr.append(td);

		var td = $("<td>");
		td.attr('class', 'datetime');
		td.html(element.datetime);
		tr.append(td);
		tr_array.push(tr);
	});

	

	for(var i=0;i<pagesNumber;i++)
	{
		// Разделяем части li, и заключаем их в div swPage:
		var table = $('<table class="table .table-hover">');
		//table.attr('class','swPage');
		var page_rows = tr_array.slice(i*resultsPerPage,(i+1)*resultsPerPage);
		page_rows.forEach(function(element, index, array){
			table.append(element);
		});	
		grid.append($('<li class="swPage">').append(table));
		// Добавляем ссылку на div swControls:
		swControls.append('<a href="" class="swShowPage">'+(i+1)+'</a>');
	}

	grid.append(swControls);
	
	var maxHeight = 0;
	var totalWidth = 0;
	
	var swPage = grid.find('.swPage');
	swPage.each(function(){
		
		// Цикл по всем созданным страницам:
		
		elem = $(this);

		var tmpHeight = 0;
		elem.find('li').each(function(){tmpHeight+=$(this).data('height');});

		if(tmpHeight>maxHeight)
			maxHeight = tmpHeight;

		elem.css('float','left').width(grid.width() - parseInt(grid.css('padding-left'))*2);
		totalWidth+=elem.outerWidth() + 20;
		console.log(totalWidth);
	});
	
	swPage.wrapAll('<div class="swSlider" />');
	
	// Установка высоты ul в значение самой высокой страницы:
	//grid.height(maxHeight);
	
	var swSlider = grid.find('.swSlider');
	swSlider.append('<div class="clear" />').width(totalWidth);

	var hyperLinks = grid.find('a.swShowPage');
	
	hyperLinks.click(function(e){
		
		// Если управляющая ссылка нажата, прокручиваем div swSlider 
		// (который содержит все страницы) и отмечаем ее как активную:

		$(this).addClass('active').siblings().removeClass('active');
		
		swSlider.stop().animate({'margin-left':-((parseInt($(this).text())-1)*(grid.width()+20))}, 0	);
		e.preventDefault();
	});
	
	// Отмечаем первую ссылку как активную при первом запуске кода:
	hyperLinks.eq(0).addClass('active');
	
	// Центрируем управляющий div:
	swControls.css({
		'left':'50%',
		'margin-left':-swControls.width()/2
	});
	
	return this;
	
}})(jQuery);


//$(document).ready(function(){
	/* Следующий код выполняется единожды сразу после загрузки DOM */
	
	// Вызов плагина jQuery и разделение списка-контейнера UL
	// на страницы по 3 li на каждой:
	
//	$('#holder').sweetPages({perPage:3});
	
	// По умлочанию плагин вставляет ссылки на страницы в ul, 
	// но нам нужно вставить их в основной контейнер:

//	var controls = $('.swControls').detach();
//	controls.appendTo('#main');
	
//});