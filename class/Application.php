<?php


namespace app;

use app\models\Db;
/**
*
*/
class Application
{
	public $db;
	private $view_path;
	private $models_path;
	public $params;
	private $controller;

	function __construct($config)
	{
		$this->view_path = $config['view_path'];
		$this->models_path = $config['models_path'];
		$this->cotrollers_path = $config['cotrollers_path'];

		$includes = [];

		$includes = scandir($this->models_path);

		foreach ($includes as $model_file_name){
			$path_info_array = pathinfo($this->models_path.$model_file_name);
			if($path_info_array['extension'] === 'php'){
				include $this->models_path.$model_file_name;
			}
		}

		$includes = scandir($this->cotrollers_path);

		foreach ($includes as $model_file_name){
			$path_info_array = pathinfo($this->models_path.$model_file_name);
			if($path_info_array['extension'] === 'php'){
				include $this->cotrollers_path.$model_file_name;
			}
		}
		$this->db = new Db($config['db']);
	}

	public function render($view_name, $args = null){

		$file_path = $this->view_path.$view_name.".php";
		$this->params[$view_name] = $args;

   	if(file_exists($file_path)){
    	$content = $file_path;
    	include $this->view_path."layout.php";
		}
	}

	public function getController($name = null)
	{
		if($name){
			$class_name = "app\controllers\\";
			$class_name .= ucfirst($name)."Controller";
			$this->controller = new $class_name();
			return $this->controller;
		}
	}

	public function getCurrentPath()
	{
		return $_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING'];
	}

	public function go()
	{
		$notfound = true;
		if(isset($_GET['r'])){
			$path_parts = explode("/", $_GET['r']);
			if(count($path_parts) == 2){
				$controller = $this->getController($path_parts[0]);
				$method_name = $controller->checkMethod($path_parts[1]);
				if($method_name !== false){
					$controller->$method_name($this);
					$notfound = false;
				}
			}
		}
		if($notfound){
			http_response_code(404);
			$this->render('404');
		}
	}
}
