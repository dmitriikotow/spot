
<div id="main">


<div id="serchmenu--block">
<div class="row">
 <div class="col-md-4">
 	<div class="sort-control">
 		<b>Sort by</b>

 		<ul>
  			<li><a href="#" onclick="sortTable('username');">User name</a></li>
  			<li><a href="#" onclick="sortTable('email');">Email</a></li>
  			<li><a href="#" onclick="sortTable('datetime');">Date/Time</a></li>
  		</ul>
  		<input type="checkbox" name="" id="ad"/> ASC / DESC
  	</div>
 </div>
 <div class="col-md-5">
 </div>
 <div class="col-md-3">
 <a id="sendmessage" href="#" class="btn" >Отправить сообщение</a>
 <a id="refresh" href="#" class="btn">Обновить</a>
 </div>
</div>
</div>

<div id="sendmessage--block" style="display: none">
<form method="post" action="javascript:void(0);" id="messageform">
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
      <label for="username"><strong>Name:</strong></label>
      <input type="text" size="50" name="username" id="username" value="" class="form-control" />
      </div>
      <div class="form-group">
      <label for="email"><strong>Email:</strong></label>
      <input type="text" size="50" name="email" id="email" value=""  class="form-control required email"/>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
      <label for="text"><strong>Text:</strong></label>
      <textarea rows="5" cols="50" name="text" id="text" class="form-control required"></textarea>
      </div>
    </div>
  </div>
<div class="form-group" align="right">
<input type="submit" value="Отправить" name="submit" class="btn btn-default"  />
<input type="button" value="Отмена" class="btn btn-default" onclick="$('#sendmessage--block').hide(500);" />
</div>
</form>
</div>
<ul id="holder">   </ul>
<span id="table-content-hidden"></span>
</div>

<script type="text/javascript">
	function getMessages(){
		$.get("index.php?r=site/jsonindex", function( data ) {
		  table_rows_obj = JSON.parse(data);
		  $('#holder').empty();
		  $('#holder').sweetPages({perPage:25, rows: table_rows_obj});
		});
	}

	function sendMessage(obj){
		var msg=$('#messageform').serialize();
		$(obj).prop("disabled", true );
	    $(obj).attr('value', 'Подождите');
		$.ajax({
			type:'POST',
	    	url:"<?php echo $_SERVER['PHP_SELF']?>?r=site/sendmessage",
			data:msg+"&action=sendmessage",
			cache:false,
			success:function(data){
				$(obj).prop("disabled", false);
	    		$(obj).attr('value', 'Отправить');
	    		$('#sendmessage--block').hide(500);
	    		getMessages();
			}
		});

	}

	function sortTable	(sort_field){
		var sort_flag = $('.sort-control').find('#ad').prop('checked');
		switch (sort_field) {
		  case 'datetime':

		    table_rows_obj.sort(function(a, b){

		    	if(parseInt(a.datetime) > parseInt(b.datetime)){
		    		return (sort_flag)?1:-1;
		    	}
		    	if(parseInt(a.datetime) < parseInt(b.datetime)){
		    		return (sort_flag)?-1:1;;
		    	}
		    	return 0;
			});
		    break;
		  case 'username':
		    table_rows_obj.sort(function(a, b){
          if(a.username == null) a.username = "";
		    	var res = a.username.localeCompare(b.username);
		    	return (sort_flag)?res : res * -1;
			});
		    break;
		  case 'email':
		    table_rows_obj.sort(function(a, b){
		    	if(a.email > b.email){
		    		return 1;
		    	}
		    	if(a.email < b.email){
		    		return -1;
		    	}
		    	return 0;
			});
		    break;
		  default:
		    alert( 'Я таких значений не знаю' );
		}
		$('#holder').empty();
		$('#holder').sweetPages({perPage:10, rows: table_rows_obj});
	}
</script>
<script type="text/javascript">
    $('a#refresh').click(function(e){
      getMessages();
    });

    $('a#sendmessage').click(function(e){
      $('#sendmessage--block').show(500);
    });

    $(document).ready(function(){
      getMessages();
      $("#messageform").validate({
        submitHandler: function(form) {
          sendMessage();
        }
      });
    });
</script>
