<div class="row">
    <div class="col-md-4">
    </div>
    <div class="col-md-4">
        <form method="post" action="<?php echo $this->getCurrentPath(); ?>" id="contactform">
        <div class="form-group">
        <label for="username"><strong>Name:</strong></label>
        <input type="text" size="50" name="username" id="username" value="" class="form-control required" />
        </div>
        <div class="form-group">
        <label for="email"><strong>Email:</strong></label>
        <input type="text" size="50" name="email" id="email" value=""  class="form-control required email"/>
        </div>
        <div class="form-group">
        <label for="text"><strong>Text:</strong></label>
        <textarea rows="5" cols="50" name="text" id="text" class="form-control required"></textarea>
        </div>
        <div class="form-group" align="right">
            <input type="submit" value="Send Message" name="submit" class="btn btn-default" />
        </div>
        </form>
    </div>
    <div class="col-md-4">
    </div>
</div>
    <script type="text/javascript">
        $(document).ready(function(){
        $("#contactform").validate();
        });
    </script>
