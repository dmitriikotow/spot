<?php
namespace app\models;

class Db
{
	public $db;

	function __construct($config)
	{
		//$this->db = new \mysqli($config['host'], $config['login'], $config['pass'], $config['dbname']);
		$this->db = new \PDO(
				"pgsql:host=${config['host']};dbname=${config['dbname']}",
				$config['login'],
				$config['pass']
		);
		//pg_connect("host=127.0.0.1 port=5432 dbname=spot user=psql_user password=14023110");
/*
		if ($this->db->connect_errno) {
		    echo "Извините, возникла проблема на сайте";
		    echo "Ошибка: Не удалсь создать соединение с базой MySQL и вот почему: \n";
		    echo "Номер_ошибки: " . $this->db->connect_errno . "\n";
		    echo "Ошибка: " . $this->db->connect_error . "\n";
		    exit;
		}
		*/
	}

	public function insert($fields, $table)
	{
		$table_keys = implode(", ", array_keys($fields));
		//$db = $this->db;
		$table_values =  array_reduce($fields, function($carry, $item){
			if(!empty($carry)) $carry .= ", ";
			$value = trim(strval($item));
			$value = (ctype_digit($value))?$value:$this->db->quote($value);
			$carry .=  $value;
			return $carry;
		});
		print_r("INSERT INTO $table($table_keys) VALUES ($table_values)");
		$this->db->query("INSERT INTO $table($table_keys) VALUES ($table_values)");
	}

	public function select($fields,$conditions, $table)
	{
		$table_keys = implode(", ", $fields);


		$compare_signs = ['=', '>=', '<=', '>', '<', 'LIKE'];
		$bool_signs = ['AND', 'OR'];
		$table_conditions = $conditions;

		$res = $this->db->query("SELECT $table_keys FROM $table WHERE $table_conditions");
		$array_data = [];
		foreach($res as $row) {
        $array_data[] = $row;
    }
		return $array_data;
	}

	public function getAllMessages(){
		$fields = ['*'];
		return $this->select($fields, 'true ORDER BY datetime DESC', 'messages');
	}

	public function validate($fields, $params){
		if(empty($fields)) return true;
		if(is_array($params) && !empty($params)){
			$diff_array = array_diff(array_keys($fields), array_keys($params));
			if(count($diff_array) !== 0) return false;

			foreach ($fields as $key => $value) {

				if(!$value['required']) continue;

				switch ($value['type']) {
					case 'varchar':
						if($params[$key] === '' || strlen($params[$key]) > 255) return false;
						break;
					case 'email':
						if($params[$key] === '') return false;
						if(!preg_match("/^[A-Z0-9._%-]+@[A-Z0-9._%-]+\.[A-Z]{2,4}$/i", trim($params[$key]))) return false;
						break;
					case 'text':

						break;
					case 'username':
						if($params[$key] === '' || strlen($params[$key]) > 255) return false;
						break;
					default:
						# code...
						break;
				}
			}

  		}else{
  			return false;
  		}

  		$result = [];
		foreach ($fields as $key => $value) {
			$result[$key] = $params[$key];
		}
  		return $result;
	}
}

?>
